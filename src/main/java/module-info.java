open module com.alphadev.jnotifier {
	requires javafx.graphics;
	requires javafx.fxml;
	requires javafx.web;
	requires java.desktop;
	requires jdk.httpserver;
}