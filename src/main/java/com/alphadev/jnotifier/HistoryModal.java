package com.alphadev.jnotifier;

import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class HistoryModal implements Initializable {

	public static final int		WIDTH		= 400;
	public static final int		HEIGHT		= 800;
	private static final String	STYLESHEET	= "/historyUI.css";
	private static final String	FXML		= "/historyUI.fxml";

	private Stage				stage;

	@FXML
	private ListView<Message>	historyList;

	public HistoryModal() {
		Rectangle screenBounds = getScreenBounds();
		Platform.setImplicitExit(false);

		stage = new Stage();
		stage.setWidth(WIDTH);
		stage.setHeight(HEIGHT);
		stage.setX((screenBounds.width - stage.getWidth()) / 2);
		stage.setY((screenBounds.height - stage.getHeight()) / 2);
		stage.setAlwaysOnTop(true);
		stage.initStyle(StageStyle.TRANSPARENT);

		FXMLLoader loader = new FXMLLoader(App.class.getResource(FXML));
		loader.setController(this);
		BorderPane decorateFram = null;
		try {
			decorateFram = loader.load();
		}
		catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		decorateFram.getStylesheets().add(STYLESHEET);
		Scene scene = new Scene(decorateFram);
		scene.getStylesheets().setAll(decorateFram.getStylesheets());
		scene.setFill(Color.TRANSPARENT);

		stage.setScene(scene);
		stage.show();

		try {
			BorderPane bp = (BorderPane) stage.getScene().getRoot();
			int margin = 2;
			double borderRadius = 0;
			try {
				margin += bp.getPadding().getBottom() * 2;
			}
			catch (NullPointerException e) {
				// No padding
			}
			try {
				margin += bp.getBorder().getStrokes().get(0).getWidths().getLeft();
				borderRadius = bp.getBorder().getStrokes().get(0).getRadii().getBottomLeftHorizontalRadius() / 2;
			}
			catch (NullPointerException e) {
				// No border
			}

			// LOG.debug("margin [{}] borderRadius[{}]", margin, borderRadius);

			javafx.scene.shape.Rectangle rect = new javafx.scene.shape.Rectangle(0, 0, stage.getWidth() - margin, stage.getHeight() - margin);
			rect.setArcHeight(borderRadius);
			rect.setArcWidth(borderRadius);
			stage.getScene().lookup("#rootMain").setClip(rect);
		}
		catch (Exception e) {
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		historyList.setItems(App.MESSAGE_HISTORIC);
		historyList.setCellFactory(i -> new MessageCell());

	}

	class MessageCell extends ListCell<Message> {
		@Override
		public void updateItem(Message item, boolean empty) {
			super.updateItem(item, empty);
			if (item == null) {
				setText(null);
			}
			else {
				setText(item.getDate() + "\n" + item.getTitle() + "\n" + item.getMessage() + "\n-----------------------");
			}
		}
	}

	private static Rectangle getScreenBounds() {
		Insets si = Toolkit.getDefaultToolkit().getScreenInsets(GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration());
		Rectangle sb = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getBounds();

		sb.x += si.left;
		sb.y += si.top;
		sb.width -= si.left + si.right;
		sb.height -= si.top + si.bottom;
		return sb;
	}

	public void onCloseWindowAction() {
		this.stage.hide();
	}

	public void show() {
		this.stage.show();

	}

}
