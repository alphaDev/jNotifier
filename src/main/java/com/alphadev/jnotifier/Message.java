package com.alphadev.jnotifier;

import java.util.Date;

public class Message {
	private String	title;
	private String	message;
	private Date	date;

	public Message(String title, String message) {
		this.title = title;
		this.message = message;
		date = new Date();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
