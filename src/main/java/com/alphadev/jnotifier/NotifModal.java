package com.alphadev.jnotifier;

import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class NotifModal implements Initializable {

	public static final int			WIDTH				= 350;
	public static final int			HEIGHT				= 200;
	private static final int		MARGING_FROM_BORDER	= 5;
	private static final String		FXML				= "/mainUI.fxml";

	private static final int		MAX_NOTIF_DISPLAY	= 4;
	private static List<NotifModal>	notifs				= new ArrayList<>();

	private Stage					stage;
	private int						index;
	private String					title;
	private String					output;

	@javafx.fxml.FXML
	private WebView					browser;
	@javafx.fxml.FXML
	private Label					titleUI;
	@javafx.fxml.FXML
	private ProgressBar				progress;

	public NotifModal(String title, String output) throws IOException {
		this.index = notifs.size();
		System.out.println("NotifModal init index [" + index + "]");
		this.title = title;
		this.output = output;

		Rectangle screenBounds = getScreenBounds();
		Platform.setImplicitExit(false);

		stage = new Stage();
		stage.setWidth(WIDTH);
		stage.setHeight(HEIGHT);
		stage.setX(screenBounds.width - MARGING_FROM_BORDER - stage.getWidth());
		stage.setY(screenBounds.height - (MARGING_FROM_BORDER + stage.getHeight()) * (index + 1));
		stage.setAlwaysOnTop(true);
		stage.initStyle(StageStyle.TRANSPARENT);

		FXMLLoader loader = new FXMLLoader(App.class.getResource(FXML));
		loader.setController(this);
		BorderPane decorateFram = loader.load();
		// decorateFram.getStylesheets().add(STYLESHEET);
		Scene scene = new Scene(decorateFram);
		// scene.setOnMousePressed(event -> {
		// xOffset = stage.getX() - event.getScreenX();
		// yOffset = stage.getY() - event.getScreenY();
		// });
		// scene.setOnMouseDragged(event -> {
		// stage.setX(event.getScreenX() + xOffset);
		// stage.setY(event.getScreenY() + yOffset);
		// });
		scene.getStylesheets().setAll(decorateFram.getStylesheets());
		scene.setFill(Color.TRANSPARENT);

		stage.setScene(scene);
		stage.show();

		try {
			BorderPane bp = (BorderPane) stage.getScene().getRoot();
			int margin = 2;
			double borderRadius = 0;
			try {
				margin += bp.getPadding().getBottom() * 2;
			}
			catch (NullPointerException e) {
				// No padding
			}
			try {
				margin += bp.getBorder().getStrokes().get(0).getWidths().getLeft();
				borderRadius = bp.getBorder().getStrokes().get(0).getRadii().getBottomLeftHorizontalRadius() / 2;
			}
			catch (NullPointerException e) {
				// No border
			}

			// LOG.debug("margin [{}] borderRadius[{}]", margin, borderRadius);

			javafx.scene.shape.Rectangle rect = new javafx.scene.shape.Rectangle(0, 0, stage.getWidth() - margin, stage.getHeight() - margin);
			rect.setArcHeight(borderRadius);
			rect.setArcWidth(borderRadius);
			stage.getScene().lookup("#rootMain").setClip(rect);
		}
		catch (Exception e) {
			System.out.println("clip error [" + e + "]");
		}

		notifs.add(this);

		if (notifs.size() > MAX_NOTIF_DISPLAY) {
			NotifModal m = notifs.remove(0);
			m.stage.close();
		}

		int i = 0;
		for (NotifModal notifModal : notifs) {
			notifModal.updateIndex(i);
			i++;
		}

	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		titleUI.setText(title);
		WebEngine webEngine = browser.getEngine();
		webEngine.loadContent(output);

		Task<Integer> task = new Task<Integer>() {
			@Override
			protected Integer call() throws Exception {
				int iterations;
				for (iterations = 0; iterations < 100; iterations++) {
					if (isCancelled()) {
						updateMessage("Cancelled");
						break;
					}
					updateMessage("Iteration " + iterations);
					updateProgress(iterations, 100);

					// Block the thread for a short time, but be sure
					// to check the InterruptedException for cancellation
					try {
						Thread.sleep(100);
					}
					catch (InterruptedException interrupted) {
						if (isCancelled()) {
							updateMessage("Cancelled");
							break;
						}
					}
				}

				Platform.runLater(() -> {
					stage.close();
					notifs.remove(index);
					int i = 0;
					for (NotifModal notifModal : notifs) {
						notifModal.updateIndex(i);
						i++;
					}
				});
				return iterations;
			}
		};

		progress.progressProperty().bind(task.progressProperty());
		new Thread(task).start();

	}

	public void updateIndex(int index) {
		Rectangle screenBounds = getScreenBounds();
		this.index = index;
		stage.setY(screenBounds.height - (MARGING_FROM_BORDER + stage.getHeight()) * (index + 1));
	}

	private static Rectangle getScreenBounds() {
		Insets si = Toolkit.getDefaultToolkit().getScreenInsets(GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration());
		Rectangle sb = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getBounds();

		sb.x += si.left;
		sb.y += si.top;
		sb.width -= si.left + si.right;
		sb.height -= si.top + si.bottom;
		return sb;
	}

	public void onCloseWindowAction() {
		this.stage.close();
		notifs.remove(index);
		int i = 0;
		for (NotifModal notifModal : notifs) {
			notifModal.updateIndex(i);
			i++;
		}

	}

}
