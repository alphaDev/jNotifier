package com.alphadev.jnotifier;

import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.Stage;

public class App extends Application {

	private static final String					APP_NAME			= "jNotifier";

	public static final ObservableList<Message>	MESSAGE_HISTORIC	= FXCollections.observableArrayList();

	private static java.awt.TrayIcon			trayIcon;

	private static HistoryModal					historyModal;

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void init() throws Exception {
		addAppToTray();

		Platform.runLater(() -> {
			historyModal = new HistoryModal();
			historyModal.onCloseWindowAction();
		});

		HttpServer server = HttpServer.create(new InetSocketAddress(3310), 0);
		server.createContext("/notifier", new MyHandler());
		server.setExecutor(null); // creates a default executor
		server.start();
		System.out.println("server start");
	}

	public static void showSystrayMessage(String title, String message) {
		if (trayIcon != null && title != null && message != null) {
			Platform.runLater(() -> {
				try {
					MESSAGE_HISTORIC.add(new Message(title, message));
					new NotifModal(title, message);
				}
				catch (IOException e) {
					e.printStackTrace();
				}
			});
		}
	}

	/**
	 * Sets up a system tray icon for the application.
	 */
	private static void addAppToTray() {
		try {
			java.awt.Toolkit.getDefaultToolkit();

			if (!java.awt.SystemTray.isSupported()) {
				System.err.println("No system tray support, application exiting.");
			}

			java.awt.SystemTray tray = java.awt.SystemTray.getSystemTray();
			java.awt.Image image = ImageIO.read(App.class.getResourceAsStream("/icon.png"));
			trayIcon = new java.awt.TrayIcon(image);

			java.awt.MenuItem openItem = new java.awt.MenuItem(APP_NAME);

			java.awt.Font defaultFont = java.awt.Font.decode(null);
			java.awt.Font boldFont = defaultFont.deriveFont(java.awt.Font.BOLD);
			openItem.setFont(boldFont);

			java.awt.MenuItem exitItem = new java.awt.MenuItem("quit");
			exitItem.addActionListener(event -> {
				tray.remove(trayIcon);
				Platform.exit();

				System.exit(0);

			});

			java.awt.MenuItem history = new java.awt.MenuItem("history");
			history.addActionListener(event -> {
				Platform.runLater(() -> {
					historyModal.show();
				});
			});

			final java.awt.PopupMenu popup = new java.awt.PopupMenu();
			popup.add(openItem);
			popup.add(history);
			popup.addSeparator();
			popup.add(exitItem);
			trayIcon.setPopupMenu(popup);

			tray.add(trayIcon);
		}
		catch (java.awt.AWTException | IOException e) {
			e.printStackTrace();
		}
	}

	public static Rectangle getScreenBounds() {
		Insets si = Toolkit.getDefaultToolkit().getScreenInsets(GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration());
		Rectangle sb = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration().getBounds();

		sb.x += si.left;
		sb.y += si.top;
		sb.width -= si.left + si.right;
		sb.height -= si.top + si.bottom;
		return sb;
	}

	@Override
	public void start(Stage stage) throws Exception {

	}

	class MyHandler implements HttpHandler {
		public Map<String, String> queryToMap(String query) {
			Map<String, String> result = new HashMap<>();
			if (query != null) {
				for (String param : query.split("&")) {
					String[] entry = param.split("=");
					if (entry.length > 1) {
						result.put(entry[0], entry[1]);
					}
					else {
						result.put(entry[0], "");
					}
				}
			}
			return result;
		}

		public void handle(HttpExchange t) throws IOException {
			// InetSocketAddress remote = t.getRemoteAddress();

			Map<String, String> params = queryToMap(t.getRequestURI().getQuery());
			String title = params.getOrDefault("title", "");
			String message = params.getOrDefault("message", "");

			System.out.println("HttpHandler call title:[" + title + "] message:[" + message + "]");

			App.showSystrayMessage(title, message);

			String response = "OK";
			t.sendResponseHeaders(200, response.length());
			OutputStream os = t.getResponseBody();
			os.write(response.getBytes());
			os.close();
		}
	}

}
